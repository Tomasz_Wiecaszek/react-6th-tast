import {BrowserRouter as Router, Switch,Route} from 'react-router-dom';
import './App.css';
import Homepage from './pages/homepage';
import {GameApp} from './context'
import Nav from './components/Nav'
import User from './pages/user'
import Game from './pages/game'

function App() {

  return (
    <div className="mainContainer">
      <Router>
  <GameApp>
    <Nav/>
      <Switch>
        <Route exact path="/" component={Homepage}/>
        <Route  path="/user" component={User}/>
        <Route  path="/game" component={Game}/>
      </Switch>
  </GameApp>
      </Router>
    </div>
  );
}

export default App;
