import {Link} from 'react-router-dom'
import './style.css';
import {GameState,} from "../../context";
import Form from "../../components/Form";
import { useContext, useEffect,useRef } from "react";

const Homepage = () => {
  //funkcja sprawdzająca czy użytkownik istnieje, jeżeli istnieje ustawia isName na true
  const { isName, setName } = useContext(GameState);
  useEffect(() => {
    const getName = localStorage.getItem("name");
    if (getName && getName.length > 0) {
      setName(true);
    }
  }, [isName]);

  return (
    <div>

      {isName?<div><p>Witamy Cię {localStorage.getItem("name")}</p>
      <button><Link to="/game">START GAME</Link></button></div>:<Form/>}
      
    </div>

  
  );
};

export default Homepage;
