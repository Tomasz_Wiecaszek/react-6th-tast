import './style.css'
import {useContext} from 'react'
import Form from '../../components/Form'
import GameWorlds from '../../components/GameWorlds'
import {GameState} from '../../context'

const Game=()=>{
    const { isName} = useContext(GameState);
return( <div>
    {isName?
   <GameWorlds/>:<Form/>
    }    
    </div>

)
}

export default Game; 