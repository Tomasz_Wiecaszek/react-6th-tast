import '../homepage/style.css'
import {useContext,React}  from 'react'
import Gamer from '../../components/Gamer'
import {UserStatsProvider,OponentStatsProvider,GameState} from '../../context'
import RandomStats from '../../components/RandomStats'
import Form from '../../components/Form'

const User=()=>{
    const { isName} = useContext(GameState);
    return(<>
        {isName?
        <div className="userWrapper">  
        <UserStatsProvider>
            <Gamer />
          </UserStatsProvider>

          <OponentStatsProvider>
            <RandomStats />
            <Gamer alive />
          </OponentStatsProvider>
        </div>:<Form/>}
        </>
    )
    }
    
    export default User; 