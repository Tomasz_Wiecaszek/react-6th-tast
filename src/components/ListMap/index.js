import React from "react";

const ListMap=({myTab})=>{
    
    return(
        myTab.map((el,index)=>{
            return <li key={index}>{el}</li>;
        })
    )
}
export default ListMap;