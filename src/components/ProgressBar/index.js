import './style.css'

const ProgressBar =(props)=>{
    let widthPcnt="60%";
    let pcts="30";
return(
    <div className='wrapper'>
            <div className='wrapper__level'>
            <p className='wrapper__level'>
            Level : {props.lvl}     
            </p>
            </div> 
            <div className='wrapper__progress'>
            
               <div className='wrapper__bar'style={{width: widthPcnt}}>
               {pcts}/100
               </div>
            </div>
        </div>
)
}
export default ProgressBar;